import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import {whitelist} from "@/public/regular";
import {getGnder, getToken} from "@/utils/getSession";
import {ElMessage} from "element-plus";
import * as constants from "constants";
import  {mainStore} from "@/store";

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect:'/login'
  },
  {
    path: '/login',
    name: 'Login',
    meta:{
      title:'登录'
    },
    component: ()=>import('@/views/Login/index.vue')
  },
  {
    path:'/reception',
    name:'Reception',
    meta:{
      title:'前台'
    },
    component: ()=>import('@/views/Reception/index.vue'),
    children:[
      {
        path:'',
        redirect:'/seat'
      },
      {
        path:'/seat',
        name:'Seat',
        meta:{
          title:'座位'
        },
        component:()=>import("@/views/Reception/Seat/index.vue")
      },
      {
        path:'/clicked',
        name:'Clicked',
        meta:{
          title:'已点'
        },
        component:()=>import("@/views/Reception/Clicked/index.vue")
      },
      {
        path:'/menu',
        name:'Menu',
        meta:{
          title:'菜单'
        },
        component:()=>import("@/views/Reception/Menu/index.vue")
      }
    ]
  },
  {
    path: '/backstage',
    name: 'Backstage',
    meta:{
      title:'后台'
    },
    component: ()=>import('@/views/Backstage/index.vue'),
    children:[
      {
        path:'',
        redirect:'/assort'
      },
      {
        path:'/assort',
        name:'Assort',
        meta:{
          title:'分类'
        },
        component:()=>import("@/views/Backstage/Assort/index.vue")
      },
      {
        path:'/dishes',
        name:'Dishes',
        meta:{
          title:'菜品'
        },
        component:()=>import("@/views/Backstage/Dishes/index.vue")
      },{
        path:'/diningTable',
        name:'DiningTable',
        meta:{
          title:'餐桌'
        },
        component:()=>import("@/views/Backstage/DiningTable/index.vue")
      },{
      path: '/orderManagement',
        name:'OrderManagement',
        meta: {
        title: '订单管理'
        },
        component:()=>import("@/views/Backstage/OrderManagement/index.vue")
      }
    ]
  },
  {
    path: '/404',
    name: 'Not',
    meta:{
      title:'错误'
    },
    component: ()=>import('@/views/404.vue')
  },
  {
    path:'/:pathMatch(.*)*',
    redirect:'/404'
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


router.beforeEach((to,from,next)=>{
  if(whitelist.includes(to.path) && !(to.path==='/login'))return next()
  if(to.path==='/login'){
    if(mainStore().token && mainStore().grade && mainStore().userid && mainStore().username)next({path: '/seat'})
    else next()
  }else{
    if(mainStore().token && mainStore().grade && mainStore().userid && mainStore().username) next()
    else next('/login')
  }
})
export default router
