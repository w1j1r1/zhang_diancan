import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import '@/assets/bootstrap.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import '@/assets/reset.css'
import dayjs from 'dayjs'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import { useIntersectionObserver } from '@vueuse/core'
//引入pinia
import {createPinia} from "pinia";
const app=createApp(App)
//element图标库
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
import 'animate.css';
//图片懒加载
app.directive('img-lazy',{
    mounted(el,binding){
        console.log(binding.value,el)
        const { stop } = useIntersectionObserver(
            el,
            ([{ isIntersecting }], observerElement) => {
               if(isIntersecting){
                   el.src=binding.value
               }
            },
        )
    }
})

const pinia=createPinia()
app.use(router)
app.use(ElementPlus)
app.use(pinia)
app.use(VueLoading);
app.mount('#app')
