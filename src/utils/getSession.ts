export const setToken=(token: string)=>{
    sessionStorage.setItem('token',token||'');
}
export const getToken=()=>{
    return sessionStorage.getItem('token')
}
export const Setusername=(username: string)=>{
    sessionStorage.setItem('username',username||'');
}
export const Setuserid=(userid:string)=>{
    return sessionStorage.setItem('userid',userid||'')
}

export const setGender=(gender:string)=>{
    sessionStorage.setItem('gender',gender||'')
}
export const getGnder=()=>{
    return sessionStorage.getItem('gender')
}
export const Getusername=()=>{
    return sessionStorage.getItem('username')
}
export const Getuserid=()=>{
    return sessionStorage.getItem('userid')
}