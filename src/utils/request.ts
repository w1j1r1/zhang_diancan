import axios from 'axios'
import {mainStore} from "@/store";
import {getToken} from "@/utils/getSession";
import {ElMessage} from "element-plus";
// @ts-ignore
axios.defaults.headers["Content-Type"] = 'application/json;charset=utf-8'
// 创建axios实例
const service = axios.create({
    // axios中请求配置有baseURL选项，表示请求URL公共部分
    baseURL:process.env["VUE_APP_BASE_API "],
    // 超时
    timeout: 10000
})
service.interceptors.request.use(res=>{
    if(getToken() && mainStore().$state.token){
        // @ts-ignore
        res.headers.token=mainStore().$state.token
    }
    return res
},err=>{
    return Promise.reject(err)
})
service.interceptors.response.use(res=>{
    const code=res.data.code

    return res.data
})
export default service