import {defineStore} from "pinia";
import {getGnder, getToken, Getuserid, Getusername} from "@/utils/getSession";
type son={
    token:string|null
    grade:string,
    username:string,
    userid:string,
    operatingId:Number
}
export const mainStore=defineStore('main',{
    state:()=>{
        return{
            token:getToken(),
            grade:getGnder(),
            username:Getusername(),
            userid:Getuserid(),
            operatingId:0
        } as son
    },
    getters:{

    },
    actions:{
        Login(data: { grade: any; id: any; token: any; username: any }){
            this.token=data.token
            this.grade=data.grade
            this.userid=data.id
            this.username=data.username
        },
        setOperatingId(tableId:Number){
            this.operatingId=tableId
        }
    }
})