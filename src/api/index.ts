import request from '@/utils/request'
// /index.php/index/login
//登录
export function login(data: { password: string; username: string }){
    return request({
        url:'index.php/index/login',
        method:'post',
        data
    })
}