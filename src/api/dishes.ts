import request from '@/utils/request'
//获取分类
export function category(){
    return request({
        url:'/houtai.php/houtai/categoryName',
        method:'get',
    })
}
//获取菜品
export function allDishes(){
    return request({
        url:'/houtai.php/houtai/allDishes',
        method:'get',
    })
}

//添加菜品
export function addDishes1(data: FormData){
    return request({
        url:'/houtai.php/houtai/addDishes',
        method:'post',
        data
    })
}
//编辑菜品
export function updateDishes(data: FormData){
    return request({
        url:'/houtai.php/houtai/updateDishes',
        method:'post',
        data
    })
}

//删除菜品
export function delDishes(data: { id: any }){
    return request({
        url:'/houtai.php/houtai/delDishes',
        method:'post',
        data
    })
}