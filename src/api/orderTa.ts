// setCurrentSta
import request from '@/utils/request'
//获取餐桌
export function setCurrentSta(data: { tableId: Number | undefined; userid: string }){
    return request({
        url:'/qiantai.php/qiantai/setCurrentSta',
        method:'post',
        data
    })
}
//获取点的菜的数量
export function showClicked(data: { indentId: String | undefined; menuId: Number | undefined }){
    return request({
        url:'/qiantai.php/qiantai/showClicked',
        method:'post',
        data
    })
}
//加菜
// aLaCarte
export function aLaCarte(data: { orderNumber: String | undefined; dishId: Number | undefined; remark: string; dishName: String | undefined; dishCount: number }){
    return request({
        url:'/qiantai.php/qiantai/aLaCarte',
        method:'post',
        data
    })
}