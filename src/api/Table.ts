import request from '@/utils/request'
//获取餐桌
export function getTableData(){
    return request({
        url:'/houtai.php/houtai/getTableData',
        method:'get',
    })
}
//获取当前餐桌
export function getCurrent(data: { userid: string }){
    return request({
        url:'/qiantai.php/qiantai/currentTable',
        method:'post',
        data
    })
}
//添加餐桌
export function addTable1(){
    return request({
        url:'/houtai.php/houtai/addTable',
        method:'get',
    })
}
//获取订单菜品以及订单号
export function serve(data: { id: number }){
    return request({
        url:'/houtai.php/houtai/serve',
        method:'post',
        data
    })
}

//上菜
export function serVegetable(data: { id: Number }){
    return request({
        url:'/houtai.php/houtai/serVegetable',
        method:'post',
        data
    })
}

//结账
export function billPlease(data: { indentId: string; tableId: number }){
    return request({
        url:'/houtai.php/houtai/billPlease',
        method:'post',
        data
    })
}


//开餐桌
export function setOrdering(data: { count: string; tableId: Number | undefined; userId: string }){
    return request({
        url:'/qiantai.php/qiantai/setOrdering',
        method:'post',
        data
    })
}

//获取全部订单  allOrderForm
export function allOrderForm(){
    return request({
        url:'/houtai.php/houtai/allOrderForm',
        method:'get'
    })
}