import request from '@/utils/request'
//获取订单号
export function showAlreadyOrderedDishes(){
    return request({
        url:'/qiantai.php/qiantai/showAlreadyOrderedDishes',
        method:'get',
    })
}
//下单
export function clickToPlaceAnOrder11(data: { particularId: string; indentId: String | undefined; tableId: Number | undefined; amountOfConsumption: any }){
    return request({
        url:'/qiantai.php/qiantai/clickToPlaceAnOrder',
        method:'post',
        data
    })
}
//显示已下单的菜 showAlreadyMenusDishes
export function showAlreadyMenusDishes(){
    return request({
        url:'/qiantai.php/qiantai/showAlreadyMenusDishes',
        method:'get',
    })
}