import request from '@/utils/request'
// houtai.php/houtai/addMenu
//展示分类列表
export function getMenus(){
    return request({
        url:'/houtai.php/houtai/categoryMenu',
        method:'get',
    })
}

interface Ref<T> {
}

//删除分类菜单
export function delMenus(data: { id: Ref<Number | String | undefined> | undefined }){
    return request({
        url:'/houtai.php/houtai/delMenu',
        method:'post',
        data
    })
}
//添加菜品
export function addMenus(data: { menuimag: String; menName: String }){
    return request({
        url:'/houtai.php/houtai/addMenu',
        method:'post',
        data
    })
}
//编辑菜品
export function updateMenus(data: { menuimag: String; menName: String; id: Number }){
    return request({
        url:'/houtai.php/houtai/updateMenu',
        method:'post',
        data
    })
}
//图标库
export function Icons(){
    return request({
        url:'/houtai.php/houtai/imagva',
        method:'get',
    })
}